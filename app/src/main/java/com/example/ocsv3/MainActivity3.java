package com.example.ocsv3;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.opencsv.CSVReader;
public class MainActivity3<strings> extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG = "EmailPassword";
    EditText emailEditText;
    EditText passwordEditText;
    SearchView searchView;
    EditText textView3;
    ArrayAdapter arrayAdapter;
    ListView listview;
    Button searchButton;
    ArrayList<Object> objectArrayList;
    ArrayList<String> foodValues;
    AutoCompleteTextView actv1;

    private static final String[] food = new String[]{"Apples", "Oranges", "Pumelo"};
    //String[] str = new String[3];
    //String[] mStrings = new String[3];

    // List<String> mStrings = new ArrayList<String>();
    // String[] strings;
    //String[] strings = new String[3];

    //strings = food.toArray(strings);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("foodTest16").child("FoodDescription");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                /* to be passed to Main Activity 3's array.

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                foodList = new ArrayList<String>();
                ArrayList<String> foodValues = new ArrayList<String>();
                foodList.add(snapshot.getKey());
                }

                */

                // april 22, 2021 -> delete all unique db values?

                ArrayList<String> foodValues = new ArrayList<String>();

                //foodValues = new ArrayList<String>();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    //Log.i("KAMLOOPS", (String) data.child(data.getKey()).getValue());
                    //Log.i("CHILDREN", data.getKey());
                    Log.i("CHILDREN", String.valueOf(data.getValue()));
                    // foodValues.add(String.valueOf(data.getValue()));
                    //foodValues.addAll(Collections.singleton(data.getKey()));
                    foodValues.add(data.getKey());
                    // final String[] food = new String[]{data.getKey()};
                    Log.i("KEY", data.getKey());  // Cheese souffle and Chicken and P // if data.getKey() (which is all foods from db already) == "Cheese souffle" (passed from autocompleteText) then get all nutrition values (below).


                   // mStrings.add(data.getKey());


                    // make data.getKey() added to ArrayList -> pass to Main Activity 3's arrayList
                    // THIS IS JUST FOR TESTING, It will happen in Main Activity3

                    //String cheeseKey = data.getKey();
                    //String cheese = data.child("NutrientID").getValue().toString(); // change name
                    //Log.i("wow", cheese);

                    GenericTypeIndicator<HashMap<String, Object>> objectsGTypeInd = new GenericTypeIndicator<HashMap<String, Object>>() {};
                    Map<String, Object> objectHashMap = data.getValue(objectsGTypeInd);
                    ArrayList<Object> objectArrayList = new ArrayList<Object>(objectHashMap.values());
                    objectArrayList.add(String.valueOf(objectArrayList.get(0)));
                    Log.i("Calories", String.valueOf(objectArrayList.get(0)));
                    Log.i("carbs", String.valueOf(objectArrayList.get(1)));
                    Log.i("pro ", String.valueOf(objectArrayList.get(2)));
                    Log.i("fat", String.valueOf(objectArrayList.get(3))); // win -> get(0) -> Calories // get(1) -> carbs // get(2) -> protein // get(3) -> Fat //

                }
                /*String[] strings = new String[mStrings.size()];
                strings = mStrings.toArray(strings);*/

                //Log.i("KAMLLOOPZ #2", String.valueOf(objectArrayList.get(1)));
                //Log.i("aaa", String.valueOf(objectArrayList.get(0)));



                //Log.i("KAMLLOOPZ", objectArrayList);

                Log.i("aaaa", foodValues.get(0));
                //Log.i("aaaa", String.valueOf(foodValues)[0]);
                //Log.i("OK", )
                // collectPhoneNumbers((Map<String,Object>) dataSnapshot.getValue());

                Map<String, Object> td = (HashMap<String,Object>) dataSnapshot.getValue();
                List<Object> values = new ArrayList<>(td.values());
                Log.i("NEW", String.valueOf(values.get(1)));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



        setContentView(R.layout.activity_main3);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final AutoCompleteTextView actv1 = findViewById(R.id.actv1);
        //final AutoCompleteTextView actv2 = findViewById(R.id.actv2);

        Button button122 = findViewById(R.id.button122);

        final TextView tv = findViewById(R.id.tv);

        // ImageView image = findViewById(R.id.image);

        actv1.setThreshold(2);
        //actv2.setThreshold(1);

        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(YourActivity.this, android.R.layout.simple_list_item_1, getPackages());
        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, food); // works
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, foodValues);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, foodValues); // works
        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity3.this,  android.R.layout.simple_dropdown_item_1line, mStrings);
        actv1.setAdapter(adapter);

        button122.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View view) {
                                             String s = actv1.getText().toString();
                                             tv.setText(s);
                                         }
                                     }
        );





            }

    // private static final String[] food = new String[]{"Apples", "Oranges"};

}

