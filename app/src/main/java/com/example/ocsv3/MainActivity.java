package com.example.ocsv3;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.opencsv.CSVReader;

public class MainActivity<mDatabase> extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG = "EmailPassword";
    EditText emailEditText;
    EditText passwordEditText;
    SearchView searchView;
    EditText textView3;
    ArrayAdapter arrayAdapter;
    ListView listview;
    Button searchButton;
    ArrayList<Object> objectArrayList;

   /* private RecyclerView recyclerView;
    private FirebaseRecyclerAdapter adapter;*/


    private List<String> foodList;
    /*private Object Map;
    private Object String;*/
   /* private MyListAdapter adapter;
    private Listview listview;
    private DatabaseReference mDatabase;*/
    //public MyListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Button button34 = findViewById(R.id.searchButton);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("Testing");


        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            //logIn();
        }

        // insertFromCSVtoDB();


      /*  FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("foodTest");
        myRef.child("FoodID").child("1").child("NutrientID").child("203").child("NutrientUnit").child("g")
                .child("NutrientSymbol").child("CARB").child("FoodDescription").child("Pummelo").child("NutrientValue")
                .setValue("38");

              */

        //Log.i("China", "doice in Create");

       /* FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("foodTest4");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String searchText) {

                // TODO Auto-generated method stub

                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchText) {

                searchText = searchView.getQuery().toString();

                firebaseUserSearch (searchText);

                return true;
            }
        });
        // return true;
    }



    private void firebaseUserSearch (final String searchText) {


        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter <Bar, ViewHolder>(

                Bar.class,
                R.layout.custom_row,
                ViewHolder.class,
                databaseReference.orderByChild("barName").startAt(searchText.toUpperCase()).endAt(searchText + "\uf8ff"))
        {
            @Override
            protected void populateViewHolder(ViewHolder viewHolder, Bar model, final int position) {

            }*/


        // *********** retrieve from database ****************************************************************************************

        /*FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("foodTest4");
        myRef.child("FoodDescription").addListenerForSingleValueEvent(new ValueEventListener() {*/

        // ^^ not in use!

        // start in use:

        /*FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("foodTest16").child("FoodDescription");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                *//* to be passed to Main Activity 3's array.

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                foodList = new ArrayList<String>();
                ArrayList<String> foodValues = new ArrayList<String>();
                foodList.add(snapshot.getKey());
                }

                *//*

                // april 22, 2021 -> delete all unique db values?

                ArrayList<String> foodValues = new ArrayList<String>();
                //foodValues = new ArrayList<String>();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    //Log.i("KAMLOOPS", (String) data.child(data.getKey()).getValue());
                    //Log.i("CHILDREN", data.getKey());
                    Log.i("CHILDREN", String.valueOf(data.getValue()));
                    foodValues.add(String.valueOf(data.getValue()));
                    Log.i("KEY", data.getKey()); // Cheese souffle and Chicken // if data.getKey() (which is all foods from db) == "Cheese souffle" (passed from autocompleteText) then get all nutrition values (below).
                    // make data.getKey() added to ArrayList -> pass to Main Activity 3's arrayList
                    // THIS IS JUST FOR TESTING, It will happen in Main Activity3

                    //String cheeseKey = data.getKey();
                    //String cheese = data.child("NutrientID").getValue().toString(); // change name
                    //Log.i("wow", cheese);

                    GenericTypeIndicator<HashMap<String, Object>> objectsGTypeInd = new GenericTypeIndicator<HashMap<String, Object>>() {};
                    Map<String, Object> objectHashMap = data.getValue(objectsGTypeInd);
                    ArrayList<Object> objectArrayList = new ArrayList<Object>(objectHashMap.values());
                    // objectArrayList.add(String.valueOf(objectArrayList.get(0)));
                    Log.i("Calories", String.valueOf(objectArrayList.get(0)));
                    Log.i("carbs", String.valueOf(objectArrayList.get(1)));
                    Log.i("pro ", String.valueOf(objectArrayList.get(2)));
                    Log.i("fat", String.valueOf(objectArrayList.get(3))); // win -> get(0) -> Calories // get(1) -> carbs // get(2) -> protein // get(3) -> Fat //

                }
                //Log.i("KAMLLOOPZ #2", String.valueOf(objectArrayList.get(1)));
                //Log.i("aaa", String.valueOf(objectArrayList.get(0)));



                //Log.i("KAMLLOOPZ", objectArrayList);

                Log.i("aaaa", foodValues.get(0));
                //Log.i("aaaa", String.valueOf(foodValues)[0]);
                //Log.i("OK", )
                // collectPhoneNumbers((Map<String,Object>) dataSnapshot.getValue());

                Map<String, Object> td = (HashMap<String,Object>) dataSnapshot.getValue();
                List<Object> values = new ArrayList<>(td.values());
                Log.i("NEW", String.valueOf(values.get(1)));
            }




            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });*/
    }

    // *********** retrieve from database END ****************************************************************************************



       public void collectPhoneNumbers(Map<String,Object> users){

            ArrayList<String> phoneNumbers = new ArrayList<>();

            //iterate through each user, ignoring their UID
            for (Map.Entry<String, Object> entry : users.entrySet()){

                //Get user map
                Map singleUser = (Map) entry.getValue();
                //Get phone field and append to list
                phoneNumbers.add((String) singleUser.get("NutrientID"));
            }

            Log.i("WINNING?", phoneNumbers.toString());
        }



        // myRef.child("FoodDescription").addValueEventListener(new ValueEventListener() {
       /* myRef.child("FoodDescription").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });*/

           /* myRef.child("FoodDescription").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    foodList = new ArrayList<String>();
                    foodList.add(snapshot.getKey());
                    ArrayList<String> foodValues = new ArrayList<String>();
                    for (DataSnapshot eventSnapshot : snapshot.getChildren()) {
                        foodList.add(eventSnapshot.getKey());
                        Log.i("testing value in ES", String.valueOf(eventSnapshot.getValue()));
                        foodValues.add(String.valueOf(eventSnapshot.getValue()));
                       *//* for (int i = 0; i < foodValues.size(); i++) {
                            Log.i("doice", foodValues.get(i));
                        }*//*
                    }
                   //foodValues.replaceAll();
                    for (String s : foodValues) {
                            Log.i("USA", String.valueOf(s.split("\\s+|(?=\\})")));
                        }

                    }
                    });*/



                    /*StringBuilder builder = new StringBuilder();
                    for (String value : foodValues) {
                        builder.append(value);
                    }
                    String text = builder.toString();
                    Log.i("USA", text);*/

    //String[] values = foodValues.split("{");
                    /*ArrayList<String> Values = new ArrayList<String>();
                    for (String s : foodValues) {
                        Values.add(String.valueOf(s.split("\\{")));
                        //Log.i("DOOOICE", String.valueOf();
                    }
                    for (String x : Values) {
                        Log.i("CANADA", x);
                    }*/


                       /* String input = "{a c df sdf TDUS^&%^7 }";
                        String regEx = "(.*[{]{2})(.*)([}]{2})";
                        Matcher matcher = Pattern.compile(regEx).matcher(s);

                        if (matcher.matches()) {
                            Log.i("THAILAND", matcher.group(2));
                        }
                    }*/


    //String getValue = String.valueOf(eventSnapshot.getValue());
    //foodValues.add(eventSnapshot.getKey()).
    //foodValues.add(String.valueOf(eventSnapshot.getValue()));
    //String[] parts = getValue.split("{");
    //Log.i("aaaa", String.valueOf(parts));


    //List myList;

    //List myList = Arrays.asList(foodValues.split(","));
                    
                   /* for (String x : myList) {
                        Log.i("xxxx in FoodValues", x);
                    }*/







            /*@Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                foodList = new ArrayList<String>();
                ArrayList<String> foodValues = new ArrayList<String>();
                foodList.add(snapshot.getKey());
                //snapshot.getKey
                Log.i("china get children", String.valueOf(snapshot.getChildren()));
                Log.i("*** testing gET KEY ***", String.valueOf(snapshot.getKey()));
                //Log.i("*** testing gET KEY ***", eventS);

                for (DataSnapshot eventSnapshot : snapshot.getChildren()) {
                    //for (DataSnapshot eventSnapshot : snapshot.getKey()) {
                    foodList.add(eventSnapshot.getKey());
                    Log.i("testing value in ES", String.valueOf(eventSnapshot.getValue()));

                    foodValues.add(String.valueOf(eventSnapshot.getValue()));
                    //Log.i("BEJING",  foodValues.get(3));
                    Log.i("TESTING ES CHILDREN", String.valueOf(eventSnapshot.getChildren()));
                    Log.i("TESTING EVENT SNAPSHOT", eventSnapshot.getKey());
                    // Log.i("*** testing 321 ***", String.valueOf(eventSnapshot.getKey()));
                }*/
                //List<String> myList = new ArrayList<String>(Arrays.asList(.split(",")));
                /*for (String s : foodValues) {
                    List<String> myList = new ArrayList<String>(Arrays.asList(s.split(",")));
                    Log.i("SUPER DUPER", String.valueOf(myList));
                }


            }
        });




                /*
                    Log.i("foodVlaues array", s);
                    for (int i = 0; i < foodValues.size(); i++) {
                        Log.i("foodValues Array", foodValues.get(i));
                    }
                }


                // adapter = new MyListAdapter(this, usersList);
                //*ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, foodList);
               // ListView listview = findViewById(R.id.myListView);
               // listview.setAdapter(arrayAdapter);*//*
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        /*
        Button searchButton = findViewById(R.id.searchButton);
        searchButton.setOnClickListener(v -> filterNow());
    }

    public void filterNow() {
        // FilterNow();
        Log.i("EXECUTED ****", "CHINAAAA");
        TextView textView3 = findViewById(R.id.textView3);
        //String text = textView3.toString().toLowerCase().trim();
        String text = textView3.getText().toString().toLowerCase().trim();
        if (TextUtils.isEmpty(text)) {
            Toast.makeText(getApplicationContext(), "searchbox is empty!", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("EXECUTED 2222****", "CHINAAAA 222");
            //arrayAdapter.getFilter().filter(text);
            try {
                Log.i("GET FILTER", text);
                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, foodList);
                ListView listview = findViewById(R.id.myListView);
                listview.setAdapter(arrayAdapter);
                arrayAdapter.getFilter().filter(text);
                listview.setAdapter(arrayAdapter);
            } catch (Exception e) {
                e.printStackTrace();
            }

            *//*ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, textView3);
            ListView listview = findViewById(R.id.myListView);
            listview.setAdapter(arrayAdapter);*//*
        }

    }
*/

        /*myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                *//*Log.i("test", String.valueOf(snapshot.getKey()));

                foodList = new ArrayList<String>();
                for (DataSnapshot eventSnapshot : snapshot.getChildren()){
                    foodList.add(eventSnapshot.getKey());
                    // Log.i("*** testing 321 ***", String.valueOf(eventSnapshot.getKey()));
                }

                // adapter = new MyListAdapter(this, usersList);
                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, foodList);
                ListView listview = findViewById(R.id.myListView);
                listview.setAdapter(arrayAdapter);
*//*

                // pass to function
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Log.i("test", String.valueOf(snapshot.getKey()));

                foodList = new ArrayList<String>();
                for (DataSnapshot eventSnapshot : snapshot.getChildren()) {
                    foodList.add(eventSnapshot.getKey());
                    // Log.i("*** testing 321 ***", String.valueOf(eventSnapshot.getKey()));
                }

                // adapter = new MyListAdapter(this, usersList);
                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, foodList);
                ListView listview = findViewById(R.id.myListView);
                listview.setAdapter(arrayAdapter);


            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }*/

        //button34.











    /*public void FilterNow() {
        String text = textView3.getText().toString().toLowerCase().trim();
        if (TextUtils.isEmpty(textView3.getText())){
            Toast.makeText(getApplicationContext(),"searchbox is empty!", Toast.LENGTH_SHORT).show();
        }else {
            arrayAdapter.getFilter().filter(text);
            listview.setAdapter(arrayAdapter);
        }
    }*/







    /*private void FilterNow() {
        String text = editText.getText().toString().toLowerCase().trim();
        if (TextUtils.isEmpty(editText.getText())){
            Toast.makeText(getContext(),"searchbox is empty!", Toast.LENGTH_SHORT).show();
        }else {
            adapter.getFilter().filter(text);
            friends.setAdapter(adapter);
        }
    }*/


        // prepArray();

        /**/

        /*
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Food");

        myRef.child("FoodID").child(nextLine[0])  // FoodID
        .child("NutrientID").child(nextLine[1]) // NutrientID
        .child("NutrientUnit").child(nextLine[2]) // NutrientUnit
        .child("NutrientSymbol").child(nextLine[3]) // NutrientSymbol
        .child("FoodDescription").child(nextLine[4]) // FoodDescription
        .child("NutrientValue").setValue(nextLine[5]) // NutrientValue

        */

        //myRef.setValue("Hello, World!");
        // onSearchConfirmed("Cheese");


        //String something = "jaja";

  /* public void onSearchConfirmed(CharSequence text) {
        //when search finish
        //Show result of search adapter
        startSearch(text.toString());
    }



    private void startSearch(String text) {
        //create query by name
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("FoodTest4");
        Query searchByName = myRef.orderByChild("FoodDescription").equalTo(text);
        Log.i("testing **", String.valueOf(searchByName));

        FirebaseRecyclerOptions<Request> foodOptions = new FirebaseRecyclerOptions.Builder<Request>()
                .setQuery(searchByName, Request.class)
                .build();

        Searchadapter = new FirebaseRecyclerAdapter<Request, OrderViewHolder>(foodOptions) {
            @Override
            protected void onBindViewHolder(@NonNull final OrderViewHolder viewHolder, final int position, @NonNull final Request model) {
                viewHolder.txtOrderId.setText(Searchadapter.getRef(position).getKey());
                viewHolder.txtOrderStatus.setText(convertCodeToStatus(model.getStatus()));
                viewHolder.txtOrderPhone.setText(model.getPhone());
                viewHolder.txtDateTime.setText(model.getDateTime());
            }
            @Override
            public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.food_item, parent, false);
                return new OrderViewHolder(itemView);
            }
        };
        Searchadapter.startListening();
        recyclerView.setAdapter(Searchadapter); //Set adapter for Recycler Vie
    }*/


        /*public void logIn () {
            Log.i("LOGIN ******", "LGIN");
            Intent myIntent = new Intent(getApplicationContext(), FirstActivity2.class);
            //.myIntent.putExtra("key", value); //Optional parameters
            startActivity(myIntent);
        }*/


            public void logIn() {
                try {
                    // Intent myIntent = new Intent(getApplicationContext(), MainActivity3.class); // WORKS
                    Intent myIntent = new Intent(getApplicationContext(), StudentTest2.class);
                    //.myIntent.putExtra("key", value); //Optional parameters
                    startActivity(myIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "China", Toast.LENGTH_SHORT).show();

                }
            }

            public void forgotPassword(View view) {
                Intent forgotPass = new Intent(getApplicationContext(), forgotPassword.class);
                //.myIntent.putExtra("key", value); //Optional parameters
                startActivity(forgotPass);
            }


            //FirebaseDatabase.getInstance();


            // CustomObject myObject = new CustomObject();

            // FirebaseAuth mAuth;

            // prepArray();

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


            public void nextClicked(View view) {
                // [START sign_in_with_email]
                EditText emailEditText = findViewById(R.id.emailEditText);
                EditText passwordEditText = findViewById(R.id.passwordEditText);

       /* Button btn = findViewById(R.id.button_first);

        int color = Color.parseColor("#ff0000");
        btn.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));*/
                //btn.setBackground(0xff99cc00);

                mAuth.signInWithEmailAndPassword(emailEditText.getText().toString().trim(), passwordEditText.getText().toString().trim())
                        .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                           /* Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);*/
                                    //Toast.makeText(getApplicationContext(), "Successful #1", Toast.LENGTH_SHORT);
                                    logIn();

                                } else {
                                    mAuth.createUserWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText.getText().toString()).addOnCompleteListener(this);
                                    if (task.isSuccessful()) {
                                        //Toast.makeText(getApplicationContext(), "Successful #2", Toast.LENGTH_SHORT);
                                        logIn();
                                    } else {
                                        //Log.i("login failed", "d");
                                        Toast.makeText(getApplicationContext(), "Login Failed, Try Again", Toast.LENGTH_SHORT);
                                    }
                                    ;
                            /*// If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Log.i("*** Testing ***", String.valueOf(emailEditText));
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);*/
                                }
                            }
                        });
            }


            //public void

            @Override
            public void onStart() {
                super.onStart();
                // Check if user is signed in (non-null) and update UI accordingly.
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if (currentUser != null) {
                    currentUser.reload();
                }
            }

            public void insertFromCSVtoDB() {

                try {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference("foodTest16");
                    CSVReader reader = new CSVReader(new InputStreamReader(getResources().openRawResource(R.raw.filename)));//Specify asset file name
                    String[] nextLine;
                    int iteration = 0;
                    while ((nextLine = reader.readNext()) != null) {

                        if (iteration == 0) {
                            iteration++;
                            continue;

                        } else {

                            /* WORKING ******** */


                            // Log.i("tesing in insert", nextLine[0]);
                            //myRef.child("FoodDescriptio").setValue(nextLine[0]);  // Was FoodID, now FoodDescriptionn
                            myRef.child("FoodDescription").child(nextLine[0]).child(nextLine[3]).setValue(nextLine[5]); // Food Description -> cheese souffle -> carbs, fat, pro, calories -> values for c,f,p,calories
                           // .child(nextLine[1]).setValue(nextLine[2]); // NutrientID
                           // myRef.child("NutrientID").setValue(nextLine[1]) ;// NutrientID
                            //myRef.child("NutrientUnit").setValue(nextLine[2]); // NutrientUnit




                    /*
                    Log.i("tesing,", nextLine[0]);

                    myRef.child("FoodDescription").child(nextLine[0])  // Was FoodID, now FoodDescription
                            .child("NutrientID").child(nextLine[1]) // NutrientID
                            .child("NutrientUnit").child(nextLine[2]) // NutrientUnit
                            .child("NutrientSymbol").child(nextLine[3]) // NutrientSymbol
                            .child("FoodID").child(nextLine[4]) // Was FoodDescription, now FoodID
                            .child("NutrientValue").setValue(nextLine[5]); // NutrientValue

                      */


                            //Log.i("TESTING", nextLine[0]);
                            // nextLine[] is an array of values from the line
                            //System.out.println(nextLine[0] + nextLine[1] + "etc...");

                            //FirebaseDatabase.getInstance().getReference().child("users").child(task.result!!.user!!.uid).child("email").setValue(emailEditText?.text.toString())
                        }
                    }
            /*Log.i("China", "doice");

            Log.i("Bejing", myRef.toString());*/
            /*myRef.child("FoodID").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DataSnapshot> task) {
                    if (!task.isSuccessful()) {
                        Log.e("firebase", "Error getting data", task.getException());
                    }
                    else {
                        Log.d("firebase", String.valueOf(task.getResult().getValue()));
                    }
                }
            });*/

            /*String queryText = "cheese";
            myRef.orderByChild("_searchLastName")
                    .startAt(queryText)
                    .endAt(queryText+"\uf8ff");*/
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "The specified file was not found", Toast.LENGTH_SHORT).show();
                }

            }


            @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.menu_main, menu);
                return true;
            }


            @Override
            public boolean onOptionsItemSelected(MenuItem item) {
                // Handle action bar item clicks here. The action bar will
                // automatically handle clicks on the Home/Up button, so long
                // as you specify a parent activity in AndroidManifest.xml.
                int id = item.getItemId();

                //noinspection SimplifiableIfStatement
                if (id == R.id.action_settings) {
                    return true;
                }

                return super.onOptionsItemSelected(item);
            }

            private void updateUI(FirebaseUser user) {

            }
        }


