package com.example.ocsv3;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class forgotPassword extends AppCompatActivity {

    EditText emailForgotPassword;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        }
        public void emailForgotPasswordButton(View view) {
            @SuppressLint("WrongViewCast") EditText emailForgotPassword = findViewById(R.id.emailForgotPassword);
            mAuth = FirebaseAuth.getInstance();
            String emailAddress = "camsimmonz@yahoo.com";
            // mAuth.sendPasswordResetEmail(emailForgotPassword.getText().toString();
            try {
                mAuth.sendPasswordResetEmail(emailAddress);
            } catch(Exception e){
                e.printStackTrace();


        }
    }
}