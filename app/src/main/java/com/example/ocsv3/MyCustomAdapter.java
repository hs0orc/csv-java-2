package com.example.ocsv3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.firebase.ui.auth.data.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MyCustomAdapter extends BaseAdapter implements ListAdapter {
    // private ArrayList<String> list = new ArrayList<String>();
    private Context context;

    List<String> mOriginalValues;
    private List<String> list = new ArrayList<String>();
    List<String> arrayList;

    ArrayList<User> users;


    public MyCustomAdapter(ArrayList<String> list, Context context) {
        this.list = list;
        this.context = context;
        //.users = users;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        //return list.get(pos).getId();
        //return list.get(pos);
        return pos;
        //just return 0 if your list items do not have an Id variable.
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.customlayout, null);
        }

        //Handle TextView and display string from your list
        TextView tvContact = (TextView) view.findViewById(R.id.tvContact);
        tvContact.setText(list.get(position));

        //Handle buttons and add onClickListeners
        //Button callbtn = (Button)view.findViewById(R.id.btn);

        Button addBtn = (Button)view.findViewById(R.id.addBtn);

        /*callbtn.setOnClickListener(new View.OnClickListener(){
=======
        Button addBtn = (Button) view.findViewById(R.id.addBtn);

       /* callbtn.setOnClickListener(new View.OnClickListener(){
>>>>>>> doice
            @Override
            public void onClick(View v) {
                //do something

            }
        });*/

        /*addBtn.setOnClickListener(new View.OnClickListener() {
                                  }*/
        addBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //do something
                notifyDataSetChanged();
                //Log.i("MYCUSTOMADAPTER", "****");
                Log.i("*******", list.get(position));

            }
        });

        return view;
    }


   /* Filter myFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            ArrayList<User> tempList = new ArrayList<User>();
            // Add the filter code here
            if (constraint != null && users != null) {
                int length = users.size();
                int i = 0;
                while (i < length) {
                    User item = users.get(i);
                    //do whatever you wanna do here
                    //adding result set output array
                    if () {  // Add check here, and fill the tempList which shows as a result

                        tempList.add(item);
                    }

                    i++;
                }
                //following two lines is very important
                //as publish result can only take FilterResults users
                filterResults.values = tempList;
                filterResults.count = tempList.size();
            }
            return filterResults;

        }


        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence contraint, FilterResults results) {
            users = (ArrayList<User>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    };


    @Override
    public Filter getFilter() {
        return myFilter;
    }*/






   /* Filter myFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            ArrayList<user> tempList=new ArrayList<user>();
            // Add the filter code here
            if(constraint != null && users != null) {
                int length= users.size();
                int i=0;
                while(i<length){
                    User item= users.get(i);
                    //do whatever you wanna do here
                    //adding result set output array

                    //item.name is user.name cause i want to search on name
                    if(item.name.toLowerCase().contains(constraint.toString().toLowerCase()) ) { // Add check here, and fill the tempList which shows as a result

                        tempList.add(item);
                    }

                    i++;
                }
                //following two lines is very important
                //as publish result can only take FilterResults users
                filterResults.values = tempList;
                filterResults.count = tempList.size();
            }
            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence contraint, FilterResults results) {
            users = (ArrayList<User>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    };

    @Override
    public Filter getFilter() {
        return myFilter;
    }
*/






    /*public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        allFoodItemlist.clear();
        if (charText.length() == 0) {
            allFoodItemlist.addAll(arraylist);
        } else {
            for (AllFoodItem wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    allFoodItemlist.add(wp);
                }
            }
        }
        notifyDataSetChanged();*/

    }




