package com.example.ocsv3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import android.view.View;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;




public class StudentTest2 extends AppCompatActivity {
    final ArrayList<food> list = new ArrayList<food>();
    public static class food {

        private String name;

        public food(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_test2);

        // insertToTextView();

        // Toast.makeText(RegWindow.this, "Fehler"+ task.getException().getMessage(), Toast.LENGTH_SHORT).show();

        AutoCompleteTextView tv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
        final ArrayList<food> list = new ArrayList<food>();
        insertToTextView("", list);
        //list.add(new food("Cheese souffle"));
        // list.add(new food("Gauti")); // add data.getkeys() to fill autocompletetextview foods
        ArrayAdapter<food> adapter = new ArrayAdapter<food>(
                this, android.R.layout.simple_dropdown_item_1line, list);
        tv.setAdapter(adapter);

        tv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            // Toast.makeText(getApplicationContext(), "Successful #1", Toast.LENGTH_SHORT);


            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                food selected = (food) arg0.getAdapter().getItem(arg2);
                Log.i("OK", selected.name);
                String s = tv.getText().toString(); // ALSO NEED TO ADD A BUTTON INCASE USER PRESSES ENTER OR BUTTON + ADD DATA TO THE AUTOCOMPLETETEXTVIEW

                // Log.i("YYAAA", String.valueOf(arg2));
                Log.i("Jffff", s);
                final ArrayList<food> empty = new ArrayList<food>();
                insertToTextView(s, empty);
                // Toast.makeText(StudentTest2.this, arg2, Toast.LENGTH_SHORT);
                /*Toast.makeText(StudentTest2.this,
                        "Clicked " + arg2 + " name: " + selected.name,
                        Toast.LENGTH_SHORT).show();*/
            }
        });


    }

    public void insertToTextView(String s, ArrayList<food> list) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("foodTest16").child("FoodDescription");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                /* to be passed to Main Activity 3's array.

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                foodList = new ArrayList<String>();
                ArrayList<String> foodValues = new ArrayList<String>();
                foodList.add(snapshot.getKey());
                }

                */

                // april 22, 2021 -> delete all unique db values?

                ArrayList<String> foodValues = new ArrayList<String>();

                //foodValues = new ArrayList<String>();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    //Log.i("KAMLOOPS", (String) data.child(data.getKey()).getValue());
                //  Log.i("Data Get Key", data.getKey()); // KEY = Food Name
                  //  Log.i("Data GET Value", String.valueOf(data.getValue())); // VALUE = MACRONUTRIENTS
                    // foodValues.add(String.valueOf(data.getValue()));
                    //foodValues.addAll(Collections.singleton(data.getKey()));
                    foodValues.add(data.getKey());
                    /*for (String s : foodValues) {
                        Log.i("TESTING123", s);
                    }*/
                    // final String[] food = new String[]{data.getKey()};
                   // Log.i("KEY", data.getKey());  // Cheese souffle and Chicken and P // if data.getKey() (which is all foods from db already) == "Cheese souffle" (passed from autocompleteText) then get all nutrition values (below).

                    list.add(new food(data.getKey()));
                    // Log.i("WINNING BEFORE", "WINNING");

                    GenericTypeIndicator<HashMap<String, Object>> objectsGTypeInd = new GenericTypeIndicator<HashMap<String, Object>>() {
                    };
                    Map<String, Object> objectHashMap = data.getValue(objectsGTypeInd);
                    ArrayList<Object> objectArrayList = new ArrayList<Object>(objectHashMap.values());
                   // objectArrayList.add(String.valueOf(objectArrayList.get(0)));
                    //objectArrayList.add(String.valueOf(objectArrayList.get(0)));
                    //int count = 0;



                    /*for (food x : list) {
                        Log.i("*** food outside x" , String.valueOf(x).trim());
                        Log.i("*** selection outside s", String.valueOf(s).trim());*/
                        //if (String.valueOf(x).equals(String.valueOf(s)))
                      if(String.valueOf(data.getKey()).equals(String.valueOf(s)))
                        {

                           // Log.i("doice", "doice");
                           // Log.i("Ready!", "Ready! aa");
                           // Log.i("food inside", String.valueOf(x).trim());
                            //Log.i("selection inside", s.trim());
                           Log.i("selection inside", String.valueOf(s).trim());

                            Log.i("Data Get Key", data.getKey()); // KEY = Food Name
                            Log.i("Data GET Value", String.valueOf(data.getValue())); // VALUE = MACRONUTRIENTS

                            objectArrayList.add(String.valueOf(objectArrayList.get(0)));
                            Log.i("food Calories", String.valueOf(objectArrayList.get(0)));
                            Log.i("food carbs", String.valueOf(objectArrayList.get(1)));
                            Log.i("food pro ", String.valueOf(objectArrayList.get(2)));
                            Log.i("food fat", String.valueOf(objectArrayList.get(3))); // win -> get(0) -> Calories // get(1) -> carbs // get(2) -> protein // get(3) -> Fat


                            Log.i("BREAKING", "BREKAING");

                            break;

                            //count = count + 1;

                            //Log.i("TESTING", String.valueOf(list.get(1)));
                            //objectArrayList.add(String.valueOf(objectArrayList.get(0)));


                            /*objectArrayList.add(String.valueOf(objectArrayList.get(0)));
                            Log.i("food Calories", String.valueOf(objectArrayList.get(0)));
                            Log.i("food carbs", String.valueOf(objectArrayList.get(1)));
                            Log.i("food pro ", String.valueOf(objectArrayList.get(2)));
                            Log.i("food fat", String.valueOf(objectArrayList.get(3))); // win -> get(0) -> Calories // get(1) -> carbs // get(2) -> protein // get(3) -> Fat
                            break;*/


                            /*
                            Log.i("food Calories", String.valueOf(objectArrayList.get(0)));
                            Log.i("food carbs", String.valueOf(objectArrayList.get(1)));
                            Log.i("food pro ", String.valueOf(objectArrayList.get(2)));
                            Log.i("food fat", String.valueOf(objectArrayList.get(3))); // win -> get(0) -> Calories // get(1) -> carbs // get(2) -> protein // get(3) -> Fat
                            */



                    }
                    //break;
                    //break;
                    //return list;
                    /*if (data.getKey() == s) {
                        Log.i("WINNING!", "WINNING!");
                    }*/
                    // mStrings.add(data.getKey());


                    // make data.getKey() added to ArrayList -> pass to Main Activity 3's arrayList
                    // THIS IS JUST FOR TESTING, It will happen in Main Activity3

                    //String cheeseKey = data.getKey();
                    //String cheese = data.child("NutrientID").getValue().toString(); // change name
                    //Log.i("wow", cheese);

                    /*
                    GenericTypeIndicator<HashMap<String, Object>> objectsGTypeInd = new GenericTypeIndicator<HashMap<String, Object>>() {
                    };
                    Map<String, Object> objectHashMap = data.getValue(objectsGTypeInd);
                    ArrayList<Object> objectArrayList = new ArrayList<Object>(objectHashMap.values());
                    objectArrayList.add(String.valueOf(objectArrayList.get(0)));
                    */
                   /* 
                   Log.i("food Calories", String.valueOf(objectArrayList.get(0)));
                    
                    Log.i("food carbs", String.valueOf(objectArrayList.get(1)));
                    Log.i("food pro ", String.valueOf(objectArrayList.get(2)));
                    Log.i("food fat", String.valueOf(objectArrayList.get(3))); // win -> get(0) -> Calories // get(1) -> carbs // get(2) -> protein // get(3) -> Fat //
                    */

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}



